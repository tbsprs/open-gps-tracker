package nl.renedegroot.android.gpstracker.ng.features.map.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Paint
import android.graphics.Shader
import android.util.AttributeSet
import android.view.View
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.asWeightedColor

class GradientView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : View(context, attrs, defStyleAttr) {

    private val orientation = Orientation.VERTICAL
    private val gradientPaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.GREEN
    }
    private val colors =
            (10 downTo 0).map {
                (it / 10f).asWeightedColor()
            }.toIntArray()

    var gradient: Boolean = true
        set(value) {
            field = value
            postInvalidate()
        }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        gradientPaint.shader = when (orientation) {
            Orientation.HORIZONTAL -> LinearGradient(
                    0f, 0f,
                    w.toFloat(), 0f,
                    colors, null, Shader.TileMode.REPEAT)
            Orientation.VERTICAL -> LinearGradient(
                    0f, 0f,
                    0f, h.toFloat(),
                    colors, null, Shader.TileMode.REPEAT)
        }
    }

    private val redPaint = Paint().apply {
        strokeWidth = 4f * resources.displayMetrics.density
        color = Color.RED
        style = Paint.Style.STROKE
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.apply {
            if (gradient) {
                drawRect(0f, 0f, width.toFloat(), height.toFloat(), gradientPaint)
            } else {
                when (orientation) {
                    Orientation.HORIZONTAL -> drawLine(0f, height / 2f, width / 2f, height / 2f, redPaint)
                    Orientation.VERTICAL -> drawLine(width / 2f, height / 2f, width / 2f, height.toFloat(), redPaint)
                }
            }
        }
    }
}

enum class Orientation {
    VERTICAL,
    HORIZONTAL
}
