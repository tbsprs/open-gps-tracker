/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSearch
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambda
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambdaDelegate
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

fun startTrackListActivity(context: Context) {
    val intent = Intent(context, TrackListActivity::class.java)
    context.startActivity(intent)
}

class TrackListActivity : AppCompatActivity(), ActivityResultLambda {

    @Inject
    lateinit var trackSearch: TrackSearch

    private val resultLambda
        get() = viewModels<ActivityResultLambdaDelegate>().value


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracklist)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.action == Intent.ACTION_SEARCH) {
            trackSearch.query.value = intent.getStringExtra(SearchManager.QUERY)
        }
    }

    override fun startActivityForResult(intent: Intent, resultHandler: (Intent?) -> Unit) {
        resultLambda.startActivityForResult(this, intent, resultHandler)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        resultLambda.onActivityResult(requestCode, resultCode, data) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}
