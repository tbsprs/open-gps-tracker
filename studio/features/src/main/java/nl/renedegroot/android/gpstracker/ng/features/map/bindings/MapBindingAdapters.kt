/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.map.bindings

import androidx.databinding.BindingAdapter
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import nl.renedegroot.android.gpstracker.ng.Instrumentation
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.ng.features.map.WeightType
import nl.renedegroot.android.gpstracker.ng.features.map.util.onMap
import nl.renedegroot.android.gpstracker.ng.features.map.widget.GradientView
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.note.NoteMarker

private fun GoogleMap.disableToolbar() {
    if (uiSettings.isMapToolbarEnabled) {
        uiSettings.isMapToolbarEnabled = false
    }
}

@BindingAdapter("polylines")
fun setPolylines(mapView: MapView, polylines: List<PolylineOptions>?) {
    val addLines: (GoogleMap) -> Unit = { googleMap ->
        googleMap.disableToolbar()
        googleMap.clear()
        polylines?.map {
            googleMap.addPolyline(it)
        }
    }
    mapView.onMap(addLines)
}

@BindingAdapter("noteMarkers")
fun setMarkers(mapView: MapView, oldMarkers: List<NoteMarker>?, newMarkers: List<NoteMarker>?) {
    val addMarkers: (GoogleMap) -> Unit = { googleMap ->
        googleMap.disableToolbar()
        oldMarkers?.map {
            it.removeFromMap()
        }
        newMarkers?.map { noteMarker ->
            noteMarker.addToMap(googleMap).apply {
                tag = noteMarker.uri
            }
        }
    }
    mapView.onMap(addMarkers)
}

@BindingAdapter("bounds", "boundsAnimation", requireAll = true)
fun setMapBounds(mapView: MapView, bounds: LatLngBounds?, animate: Boolean?) {
    if (bounds != null) {
        mapView.onMap {
            val padding = mapView.context.resources.getDimension(R.dimen.map_padding)
            val update = CameraUpdateFactory.newLatLngBounds(bounds, padding.toInt())
            mapView.changeCamera(it, update, animate = animate ?: true)
        }
    }
}

@BindingAdapter("focus")
fun setMapTarget(mapView: MapView, focus: LatLng?) {
    if (focus != null) {
        mapView.onMap {
            val cameraFocus = com.google.android.gms.maps.model.LatLng(focus.latitude, focus.longitude)
            if (it.cameraPosition.zoom < OVERVIEW || it.cameraPosition.zoom > CLOSE_UP) {
                mapView.changeCamera(it, CameraUpdateFactory.newLatLngZoom(cameraFocus, CLOSE_UP))
            } else if (!it.projection.visibleRegion.latLngBounds.contains(cameraFocus)) {
                mapView.changeCamera(it, CameraUpdateFactory.newLatLng(cameraFocus))
            }
        }
    }
}

private fun MapView.changeCamera(googleMap: GoogleMap, update: CameraUpdate, animate: Boolean = true) {
    if (height > 0 && width > 0) {
        if (should(animate)) {
            googleMap.animateCamera(update, null)
        } else {
            googleMap.moveCamera(update)
        }
    }
}

@BindingAdapter("satellite")
fun setSatellite(map: MapView, showSatellite: Boolean) {
    map.onMap {
        if (showSatellite) {
            it.mapType = MAP_TYPE_HYBRID
        } else {
            it.mapType = MAP_TYPE_NORMAL
        }
    }
}

@BindingAdapter("gradient")
fun setGradient(view: GradientView, weight: WeightType) {
    when (weight) {
        WeightType.Speed -> view.gradient = true
        WeightType.Altitude -> view.gradient = true
        WeightType.None -> view.gradient = false
    }
}

@BindingAdapter("onMapClick")
fun setOnMapClick(map: MapView, clickListener: GoogleMap.OnMapClickListener) {
    map.onMap {
        it.setOnMapClickListener(clickListener)
    }
}

@BindingAdapter("onMapLongClick")
fun setOnMapLongClick(map: MapView, clickListener: GoogleMap.OnMapLongClickListener) {
    map.onMap {
        it.setOnMapLongClickListener(clickListener)
    }
}

@BindingAdapter("onMarkerLongClick")
fun setOnMarkerLongClick(map: MapView, clickListener: GoogleMap.OnMarkerClickListener) {
    map.onMap {
        it.setOnMarkerClickListener(clickListener)
    }
}

private const val OVERVIEW = 10.0F
private const val CLOSE_UP = 15.0F

private fun should(animate: Boolean): Boolean = if (Instrumentation.active) {
    false
} else {
    animate
}