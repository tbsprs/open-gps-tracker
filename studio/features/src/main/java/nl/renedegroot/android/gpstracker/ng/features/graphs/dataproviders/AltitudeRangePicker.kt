/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences

class AltitudeRangePicker(val unitSettingsPreferences: UnitSettingsPreferences) {

    fun prettyMinYValue(yValue: Float): Float {
        val conversion = unitSettingsPreferences.meterToSmallDistance

        val height = yValue / conversion

        return 200 * (height.toInt() / 200) * conversion
    }

    fun prettyMaxYValue(yValue: Float): Float {
        val conversion = unitSettingsPreferences.meterToSmallDistance
        val height = yValue / conversion

        return 200 * (1 + height.toInt() / 200) * conversion
    }
}
