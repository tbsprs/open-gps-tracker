/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.model

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.annotation.WorkerThread
import androidx.core.content.edit
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import nl.renedegroot.android.gpstracker.ng.base.dagger.DiskIO
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureScope
import nl.renedegroot.android.gpstracker.ng.features.map.WeightType
import java.util.concurrent.Executor
import javax.inject.Inject

private const val WAKE_LOCK_SCREEN = "WAKE_LOCK_SCREEN"
private const val SATELLITE = "SATELLITE"
private const val WEIGHT_TYPE = "WEIGHT_TYPE"

@FeatureScope
class Preferences @Inject constructor(context: Context, @DiskIO private val executor: Executor) {

    val wakelockScreen = MutableLiveData<Boolean>()
    val satellite = MutableLiveData<Boolean>()
    val weightType = MutableLiveData<WeightType>()
    private lateinit var preferences: SharedPreferences
    private val preferenceListener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
        when (key) {
            WAKE_LOCK_SCREEN -> executor.execute { readWakeLockScreen() }
            SATELLITE -> executor.execute { readSatellite() }
        }
    }
    private val wakelockScreenObserver = Observer<Boolean> {
        wakelockScreen.storeAsBooleanPreference(WAKE_LOCK_SCREEN)
    }
    private val satelliteObserver = Observer<Boolean> {
        satellite.storeAsBooleanPreference(SATELLITE)
    }
    private val weightObserver = Observer<WeightType> {
        weightType.storeAsWeightPreference(WEIGHT_TYPE)
    }

    init {
        executor.execute {
            preferences = context.getSharedPreferences("settings", MODE_PRIVATE)
            preferences.registerOnSharedPreferenceChangeListener(preferenceListener)

            readWakeLockScreen()
            readSatellite()
            readWeightType()
        }
        wakelockScreen.observeForever(wakelockScreenObserver)
        satellite.observeForever(satelliteObserver)
        weightType.observeForever(weightObserver)
    }

    @WorkerThread
    private fun readSatellite() {
        satellite.postValue(preferences.getBoolean(SATELLITE, false))
    }

    @WorkerThread
    private fun readWakeLockScreen() {
        wakelockScreen.postValue(preferences.getBoolean(WAKE_LOCK_SCREEN, false))
    }

    @WorkerThread
    private fun readWeightType() {
        val value = preferences.getInt(WEIGHT_TYPE, 0)
        val type = when (value) {
            0 -> WeightType.Speed
            1 -> WeightType.Altitude
            2 -> WeightType.None
            else -> WeightType.Speed
        }
        weightType.postValue(type)
    }

    private fun MutableLiveData<Boolean>.storeAsBooleanPreference(key: String) {
        val storedValue = preferences.getBoolean(key, false)
        val value = valueOrFalse()
        if (storedValue != value) {
            preferences.edit {
                putBoolean(key, value)
            }
        }
    }

    private fun MutableLiveData<WeightType>.storeAsWeightPreference(key: String) {
        val storedValue = preferences.getInt(key, 0)
        val newValue = when (value) {
            WeightType.Speed -> 0
            WeightType.Altitude -> 1
            WeightType.None -> 2
            null -> 0
        }
        if (storedValue != newValue) {
            preferences.edit {
                putInt(key, newValue)
            }
        }
    }
}

fun MutableLiveData<Boolean>.valueOrFalse(): Boolean = this.value ?: false

fun MutableLiveData<Boolean>.not() =
        this.valueOrFalse().not()
                .also { this.value = it }

fun MutableLiveData<WeightType>.toggle() =
        this.value?.toggle()
                .also { this.value = it }
