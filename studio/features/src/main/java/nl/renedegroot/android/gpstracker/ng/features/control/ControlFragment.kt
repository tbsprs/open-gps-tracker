/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.control

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentControlBinding

/**
 * On screen controls for the logging state
 */
class ControlFragment : Fragment() {

    private val controlPresenter: ControlPresenter
        get() = ViewModelProvider(this).get(ControlPresenter::class.java)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        ControlNavigator(this)
                .observe(controlPresenter.navigation)

        return DataBindingUtil.inflate<FragmentControlBinding>(inflater, R.layout.fragment_control, container, false).apply {
            presenter = controlPresenter
            viewModel = controlPresenter.viewModel
        }.root
    }


    override fun onStart() {
        super.onStart()
        controlPresenter.start()
    }

    override fun onStop() {
        super.onStop()
        controlPresenter.stop()
    }
}
