/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.util

import androidx.lifecycle.Observer
import android.net.Uri
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.service.util.readName
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractTrackPresenter

abstract class AbstractSelectedTrackPresenter : AbstractTrackPresenter() {

    protected val trackSelection = FeatureConfiguration.featureComponent.trackSelection()
    private val selectionObserver = Observer<Uri> { trackUri -> onTrackSelected(trackUri) }

    override suspend fun onStart() {
        super.onStart()
        trackSelection.selection.observeForever(selectionObserver)
    }

    override suspend fun onStop() {
        trackSelection.selection.removeObserver(selectionObserver)
        super.onStop()
    }

    private fun onTrackSelected(trackUri: Uri?) {
        super.trackUri = trackUri
    }

    final override suspend fun onChange() {
        val trackName = trackUri?.readName() ?: ""
        onTrackUpdate(trackUri, trackName)
    }

    abstract fun onTrackUpdate(trackUri: Uri?, name: String)
}
