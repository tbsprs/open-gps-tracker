/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import androidx.annotation.StringRes
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import nl.renedegroot.android.opengpstrack.ng.features.R

interface GraphDataCalculator {

    val graphPoint: List<GraphPoint>

    @WorkerThread
    fun calculateGraphPoints(summary: Summary)

    @get:StringRes
    val xLabel: Int
    @get:StringRes
    val yLabel: Int

    fun describeXvalue(context: Context, xValue: Float): String {
        return ""
    }

    fun describeYvalue(context: Context, yValue: Float): String {
        return ""
    }

    fun prettyMinYValue(yValue: Float): Float

    fun prettyMaxYValue(yValue: Float): Float

    object DefaultGraphValueDescriptor : GraphDataCalculator {

        override val graphPoint: List<GraphPoint>
            get() = emptyList()

        override fun calculateGraphPoints(summary: Summary) {}

        override fun prettyMinYValue(yValue: Float) = yValue

        override fun prettyMaxYValue(yValue: Float) = yValue

        override val xLabel: Int
            get() = R.string.missing_value
        override val yLabel: Int
            get() = R.string.missing_value
    }
}
