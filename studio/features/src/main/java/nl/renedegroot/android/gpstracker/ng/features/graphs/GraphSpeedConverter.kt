/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs

import javax.inject.Inject

class GraphSpeedConverter @Inject constructor() {

    fun speedToYValue(meterPerSecond: Float): Float {
        if (meterPerSecond < 0.001) {
            return 0F
        }
        return (1F / meterPerSecond) / 0.06F
    }

    fun yValueToSpeed(minutePerKilometer: Float): Float {
        val kilometerPerMinute = 1F / minutePerKilometer
        return kilometerPerMinute * 1000 / 60
    }
}
