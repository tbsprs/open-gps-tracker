/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import javax.inject.Inject

class AltitudeOverDistanceDataProvider() : GraphDataCalculator {

    @Inject
    lateinit var statisticsFormatter: StatisticsFormatter

    @Inject
    lateinit var graphSpeedConverter: GraphSpeedConverter

    @Inject
    lateinit var speedRangePicker: AltitudeRangePicker

    private var _graphPoint: List<GraphPoint> = emptyList()

    override val graphPoint: List<GraphPoint>
        get() = _graphPoint

    override val yLabel: Int
        get() = R.string.graph_label_height

    override val xLabel: Int
        get() = R.string.graph_label_distance

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun prettyMinYValue(yValue: Float) =
            speedRangePicker.prettyMinYValue(yValue)

    override fun prettyMaxYValue(yValue: Float) =
            speedRangePicker.prettyMaxYValue(yValue)

    @WorkerThread
    override fun calculateGraphPoints(summary: Summary) {
        val graphPoints = mutableListOf<GraphPoint>()
        summary.deltas.forEach {
            addSegmentToGraphPoints(it, graphPoints)
        }
        _graphPoint = graphPoints
    }

    override fun describeYvalue(context: Context, yValue: Float): String {
        return statisticsFormatter.convertMetersAltitude(context, yValue)
    }

    override fun describeXvalue(context: Context, xValue: Float): String {
        return statisticsFormatter.convertMetersToDistance(context, xValue)
    }

    private fun addSegmentToGraphPoints(deltas: List<Summary.Delta>, graphPoints: MutableList<GraphPoint>) {
        deltas.forEach {
            if (it.altitude >= 0F && it.deltaMeters > 0F) {
                val distance = it.totalMeters - (it.deltaMeters / 2F)
                graphPoints.add(GraphPoint(distance, it.altitude.toFloat()))
            }
        }
    }
}

