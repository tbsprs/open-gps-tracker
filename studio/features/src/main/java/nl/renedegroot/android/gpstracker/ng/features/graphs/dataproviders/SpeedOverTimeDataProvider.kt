/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import android.content.Context
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.GraphSpeedConverter
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.GraphPoint
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import nl.renedegroot.android.opengpstrack.ng.summary.summary.speed
import javax.inject.Inject

class SpeedOverTimeDataProvider(
        private val unitSettingsPreferences: UnitSettingsPreferences,
        private val isRunning: Boolean) : GraphDataCalculator {

    @Inject
    lateinit var statisticsFormatter: StatisticsFormatter

    @Inject
    lateinit var graphSpeedConverter: GraphSpeedConverter

    private var _graphPoint: List<GraphPoint> = emptyList()
    override val graphPoint: List<GraphPoint>
        get() = _graphPoint

    private val speedRangePicker = SpeedRangePicker(unitSettingsPreferences, isRunning)

    override val yLabel: Int
        get() = R.string.graph_label_speed

    override val xLabel: Int
        get() = R.string.graph_label_time

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun prettyMinYValue(yValue: Float) =
            speedRangePicker.prettyMinYValue(yValue)

    override fun prettyMaxYValue(yValue: Float) =
            speedRangePicker.prettyMaxYValue(yValue)

    @WorkerThread
    override fun calculateGraphPoints(summary: Summary) {
        val graphPoints = mutableListOf<GraphPoint>()
        addSegmentToGraphPoints(summary.deltas, graphPoints)

        val milliseconds = 40_000F
        _graphPoint = graphPoints
                .inverseSpeed()
                .flattenOutliers()
                .smooth(milliseconds)
    }


    override fun describeYvalue(context: Context, yValue: Float): String {
        val speed = if (isRunning && unitSettingsPreferences.useInverseRunningSpeed) yValue.inverseSpeed() else yValue
        return statisticsFormatter.convertMeterPerSecondsToSpeed(context, speed, isRunning, decimals = false)
    }

    override fun describeXvalue(context: Context, xValue: Float): String {
        return statisticsFormatter.convertSpanDescriptiveDuration(context, xValue.toLong())
    }

    private fun addSegmentToGraphPoints(waypoints: List<List<Summary.Delta>>, graphPoints: MutableList<GraphPoint>) {
        val baseTime = waypoints.firstOrNull()?.firstOrNull()?.totalMilliseconds ?: 0L
        waypoints.forEach {
            val points = calculateSegment(it, baseTime)
            graphPoints.addAll(points)
        }
    }

    fun calculateSegment(deltas: List<Summary.Delta>, baseTime: Long): List<GraphPoint> {
        val list = mutableListOf<GraphPoint>()
        fun Long.toX() = (this - baseTime).toFloat()
        deltas.forEach {
            val speed = it.speed()
            if (speed >= 0F) {
                val moment = it.totalMilliseconds - (it.deltaMilliseconds / 2L)
                list.add(GraphPoint(moment.toX(), speed))
            }
        }
        return list
    }

    private fun List<GraphPoint>.inverseSpeed(): List<GraphPoint> {
        return if (isRunning && unitSettingsPreferences.useInverseRunningSpeed) {
            this.map { GraphPoint(it.x, it.y.inverseSpeed()) }
        } else {
            this
        }
    }

    private fun Float.inverseSpeed() = graphSpeedConverter.speedToYValue(this)
}
