/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map.rendering

import android.graphics.Color
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions

private const val LINE_SEGMENTS = 75

class TrackPolylineProvider(val waypoints: List<List<LatLng>>) {

    val lineOptions by lazy { drawPolylines() }

    private fun drawPolylines(): List<PolylineOptions> {
        val points = waypoints.filter { it.size >= 2 }
        val distribution = distribute(points)
        val lineOptions = mutableListOf<PolylineOptions>()
        for (i in points.indices) {
            val options = PolylineOptions()
                    .width(5f)
                    .color(Color.RED)
            fillLine(points[i], options, distribution[i])
            lineOptions.add(options)
        }

        return lineOptions
    }

    private fun distribute(points: List<List<LatLng>>): List<Int> {
        val distribution = mutableListOf<Int>()
        val total = points.fold(0) { count, list -> count + list.size }
        points.forEach { distribution.add((LINE_SEGMENTS * it.size) / total) }

        return distribution
    }

    private fun fillLine(points: List<LatLng>, options: PolylineOptions, goal: Int) {
        options.add(points.first())
        if (goal > 0) {
            val initialStep = points.size / goal
            val step = Math.max(1, initialStep)
            for (i in step until points.size step step) {
                options.add(points[i])
            }
        }
        options.add(points.last())
    }
}
