/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.content.Context
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import nl.renedegroot.android.gpstracker.ng.base.location.LocationFactory
import nl.renedegroot.android.gpstracker.permissions.PermissionChecker
import nl.renedegroot.android.gpstracker.permissions.PermissionHelper
import nl.renedegroot.android.opengpstrack.ng.features.R
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnit
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

class NameGeneratorTest {

    private lateinit var sut: NameGenerator

    @get:Rule
    var rule = MockitoJUnit.rule()

    val format: SimpleDateFormat = mock()

    val now: Calendar = mock {
        whenever(it.time).thenReturn(Date())
    }

    val context: Context = mock()

    val locationFactory: LocationFactory = mock {}

    val checker: PermissionChecker = mock {
        whenever(it.checkSelfPermission(eq(context), any<List<String>>())).thenReturn(true)
        whenever(it.checkSelfPermission(eq(context), any<String>())).thenReturn(true)
    }

    val permissionHelper = PermissionHelper(context, checker, mock())

    @Before
    fun setUp() {
        `when`(context.getString(R.string.period_morning)).thenReturn("morning")
        `when`(context.getString(R.string.period_afternoon)).thenReturn("afternoon")
        `when`(context.getString(R.string.period_evening)).thenReturn("evening")
        `when`(context.getString(R.string.period_night)).thenReturn("night")
        `when`(context.getString(eq(R.string.initial_time_track_name), anyString(), anyString())).then {
            "${it.arguments[1]} ${it.arguments[2]}"
        }
        `when`(context.getString(eq(R.string.initial_time_location_track_name), anyString(), anyString(), anyString())).then {
            "${it.arguments[1]} ${it.arguments[2]} in ${it.arguments[3]}"
        }

        sut = NameGenerator(context, format, locationFactory, permissionHelper)
    }

    @Test
    fun generateSaterdayNight() {
        // Arrange
        `when`(now.get(Calendar.HOUR_OF_DAY)).thenReturn(1)
        `when`(format.format(any())).thenReturn("Saturday")
        // Act
        val name = sut.generateName(now)
        // Assert
        assertThat(name, `is`("Saturday night"))
    }

    @Test
    fun generateMondayMorning() {
        // Arrange
        `when`(now.get(Calendar.HOUR_OF_DAY)).thenReturn(7)
        `when`(format.format(any())).thenReturn("Monday")
        // Act
        val name = sut.generateName(now)
        // Assert
        assertThat(name, `is`("Monday morning"))
    }

    @Test
    fun generateWednesdayAfternoon() {
        // Arrange
        `when`(now.get(Calendar.HOUR_OF_DAY)).thenReturn(14)
        `when`(format.format(any())).thenReturn("Wednesday")
        // Act
        val name = sut.generateName(now)
        // Assert
        assertThat(name, `is`("Wednesday afternoon"))
    }

    @Test
    fun generateSundayEvening() {
        // Arrange
        `when`(now.get(Calendar.HOUR_OF_DAY)).thenReturn(21)
        `when`(format.format(any())).thenReturn("Sunday")
        // Act
        val name = sut.generateName(now)
        // Assert
        assertThat(name, `is`("Sunday evening"))
    }


    @Test
    fun generateSundayEveningAmsterdam() {
        // Arrange
        `when`(now.get(Calendar.HOUR_OF_DAY)).thenReturn(21)
        `when`(format.format(any())).thenReturn("Sunday")
        `when`(locationFactory.getLocationName()).thenReturn("Amsterdam")
        // Act
        val name = sut.generateName(now)
        // Assert
        assertThat(name, `is`("Sunday evening in Amsterdam"))
    }
}
