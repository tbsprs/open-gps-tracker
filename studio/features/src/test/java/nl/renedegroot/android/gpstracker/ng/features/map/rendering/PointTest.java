/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map.rendering;

import junit.framework.Assert;

import org.junit.Test;

public class PointTest {

    @Test
    public void testDistancePositiveRight() {
        // Setup
        Point a = new Point(10, 10);
        Point b = new Point(12, 12);

        // Execute
        double distanceSquared = a.squaredDistanceTo(b);

        // Verify
        Assert.assertEquals(4, distanceSquared, 0.000001);
    }

    @Test
    public void testDistancePositiveLeft() {
        // Setup
        Point a = new Point(10, 10);
        Point b = new Point(12, 12);

        // Execute
        double distanceSquared = b.squaredDistanceTo(a);

        // Verify
        Assert.assertEquals(4, distanceSquared, 0.000001);
    }

    @Test
    public void testDistanceNegativeRight() {
        // Setup
        Point a = new Point(-1, -1);
        Point b = new Point(1, 1);

        // Execute
        double distanceSquared = a.squaredDistanceTo(b);

        // Verify
        Assert.assertEquals(4, distanceSquared, 0.000001);
    }

    @Test
    public void testDistanceNegativeLeft() {
        // Setup
        Point a = new Point(1, 1);
        Point b = new Point(-1, -1);

        // Execute
        double distanceSquared = b.squaredDistanceTo(a);

        // Verify
        Assert.assertEquals(4, distanceSquared, 0.000001);
    }
}
