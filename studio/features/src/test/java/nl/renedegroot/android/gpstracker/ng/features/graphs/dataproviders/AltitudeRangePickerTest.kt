package nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test

const val A_200_FEET_IN_METER = 60.96F
const val A_213_FEET_IN_METER = 64.9224F
const val A_198_FEET_IN_METER = 60.3504F

class AltitudeRangePickerTest {

    val unitSettingsPreferences: UnitSettingsPreferences = mock()

    private fun imperialResources() {
        whenever(unitSettingsPreferences.meterToSmallDistance).thenReturn(0.30479999953F)
    }

    private fun metricResources() {
        whenever(unitSettingsPreferences.meterToSmallDistance).thenReturn(1F)
    }

    val sut = AltitudeRangePicker(unitSettingsPreferences)

    @Test
    fun `couple of meter to zero`() {
        metricResources()
        val pretty = sut.prettyMinYValue(3F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `near 100 of meter to zero`() {
        metricResources()
        val pretty = sut.prettyMinYValue(99F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `over 200 of meter to 200`() {
        metricResources()
        val pretty = sut.prettyMinYValue(229F)

        assertThat(pretty, `is`(200F))
    }

    @Test
    fun `couple of feet to zero`() {
        imperialResources()
        val pretty = sut.prettyMinYValue(3F)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `near 200 of feet to zero`() {
        imperialResources()
        val pretty = sut.prettyMinYValue(A_198_FEET_IN_METER)

        assertThat(pretty, `is`(0F))
    }

    @Test
    fun `over 200 of feet to 200`() {
        imperialResources()
        val pretty = sut.prettyMinYValue(A_213_FEET_IN_METER)

        assertThat(pretty, `is`(A_200_FEET_IN_METER))
    }
}
