/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target

import android.app.Activity
import android.content.ContentResolver
import android.net.Uri

internal interface ExportTarget {

    fun checkConnection(activity: Activity, onConnected: (Connection) -> Unit)

    fun requestConnection(activity: Activity, onConnected: (Connection) -> Unit)

    fun removeConnection(activity: Activity)

    fun createUploadTask(contentResolver: ContentResolver, trackUri: Uri, callback: Callback): Executable
}