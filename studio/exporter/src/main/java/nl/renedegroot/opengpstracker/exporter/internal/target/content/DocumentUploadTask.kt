/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target.content

import android.content.ContentResolver
import android.net.Uri
import android.provider.DocumentsContract
import android.provider.DocumentsContract.Document.COLUMN_DISPLAY_NAME
import android.provider.DocumentsContract.Document.COLUMN_DOCUMENT_ID
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.isActive
import nl.renedegroot.android.gpstracker.utils.contentprovider.map
import nl.renedegroot.opengpstracker.exporter.GpxCreator
import nl.renedegroot.opengpstracker.exporter.internal.target.Callback
import nl.renedegroot.opengpstracker.exporter.internal.target.Executable
import timber.log.Timber

internal class DocumentUploadTask(
        private val contentResolver: ContentResolver,
        private val trackUri: Uri,
        private val callback: Callback,
        private val contentUri: Uri
) : Executable {

    private val gpxCreator = GpxCreator(contentResolver, trackUri)
    private val filename: String by lazy { GpxCreator.fileName(trackUri, "gpx") }

    override suspend fun execute() = coroutineScope {
        Timber.d("Looking for export directory file $filename")
        val directoryId = DocumentsContract.getTreeDocumentId(contentUri)
        if (!isActive) {
            return@coroutineScope
        }
        deletePrevious(directoryId)
        if (!isActive) {
            return@coroutineScope
        }
        createFile(directoryId)
        if (isActive) {
            callback.onFinished(trackUri)
        }
    }

    private fun deletePrevious(directoryId: String?) {
        Timber.d("Looking for GPX file $filename")
        val directory = DocumentsContract.buildDocumentUriUsingTree(contentUri, directoryId)
        val documentUri = directory.documentForChildFilename(contentResolver, filename)
        documentUri?.let {
            Timber.d("Deleting previous file $filename")
            DocumentsContract.deleteDocument(contentResolver, documentUri)
        }
    }

    private fun createFile(directoryId: String) {
        Timber.d("Creating GPX file $filename")
        val directory = DocumentsContract.buildDocumentUriUsingTree(contentUri, directoryId)
        val gpxFile = DocumentsContract.createDocument(
                contentResolver,
                directory,
                GpxCreator.MIME_TYPE_GPX,
                filename
        )
        Timber.d("Creating GPX content")
        contentResolver.openOutputStream(gpxFile)?.use {
            gpxCreator.createGpx(it)
        }
    }
}

private fun Uri.documentForChildFilename(contentResolver: ContentResolver, fileName: String): Uri? {
    val childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(this, DocumentsContract.getDocumentId(this))
    val documentId = childrenUri.map(
            contentResolver,
            projection = listOf(COLUMN_DOCUMENT_ID, COLUMN_DISPLAY_NAME)
    ) {
        if (it.getString(1).sameLetterOrDigit(fileName)) {
            it.getString(0)
        } else {
            null
        }
    }.find { it != null }
    return if (documentId != null) {
        DocumentsContract.buildDocumentUriUsingTree(this, documentId)
    } else {
        null
    }
}

private fun String.sameLetterOrDigit(other: String): Boolean =
        this.length == other.length &&
                this.foldIndexed(true) { index, acc, character ->
                    if (character.isLetterOrDigit() || other[index].isLetterOrDigit()) {
                        acc && character == other[index]
                    } else {
                        acc
                    }
                }
