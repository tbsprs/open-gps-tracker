package nl.renedegroot.android.gpstracker.utils.preference

import android.content.Context
import android.content.SharedPreferences
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.FiveMinutes
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.Second
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.TenSeconds
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.values
import kotlin.math.max
import kotlin.math.min

private const val SETTINGS_LOGGING_PREFERENCE = "logging_settings"

private const val SETTINGS_CUSTOM_INTERVAL = "custom interval"
private const val SETTINGS_CUSTOM_INTERVAL_DEFAULT = false

private const val SETTINGS_CUSTOM_INTERVAL_PERIOD = "custom interval period"
val SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT = TenSeconds.intervalTime

class LoggingSettingsPreferences(context: Context) {

    private val appContext = context.applicationContext
    private val preferences by lazy { appContext.getSharedPreferences(SETTINGS_LOGGING_PREFERENCE, Context.MODE_PRIVATE) }
    private var customLoggingInterval by intPreference(appContext, SETTINGS_LOGGING_PREFERENCE, SETTINGS_CUSTOM_INTERVAL_PERIOD, SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT)
    var useCustomInterval by booleanPreference(appContext, SETTINGS_LOGGING_PREFERENCE, SETTINGS_CUSTOM_INTERVAL, SETTINGS_CUSTOM_INTERVAL_DEFAULT)
    var loggingInterval: LoggingInterval
        get() = values().firstOrNull { it.intervalTime == customLoggingInterval }
                ?: TenSeconds
        set(value) {
            customLoggingInterval = value.intervalTime
        }

    private val preferenceChangeListener = object : SharedPreferences.OnSharedPreferenceChangeListener {
        var onChanges: (() -> Unit)? = null
        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            onChanges?.invoke()
        }
    }

    fun observeChanges(function: () -> Unit) {
        preferenceChangeListener.onChanges = function
        preferences.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    fun ignoreChanges() {
        preferences.unregisterOnSharedPreferenceChangeListener(preferenceChangeListener)
    }
}

enum class LoggingInterval(
        val intervalTime: Int
) {
    Second(1),
    TenSeconds(10),
    HalfMinute(30),
    Minute(60),
    FiveMinutes(60 * 5);

    override fun toString() = "Every $intervalTime seconds"
}

fun clampInterval(interval: Int) = clamp(Second.intervalTime, interval, FiveMinutes.intervalTime)

val LoggingSettingsPreferences.activeInterval
    get() = if (useCustomInterval) {
        loggingInterval.intervalTime
    } else {
        SETTINGS_CUSTOM_INTERVAL_PERIOD_DEFAULT
    }

private fun clamp(lowerBound: Int, interval: Int, upperBound: Int) = min(max(lowerBound, interval), upperBound)

