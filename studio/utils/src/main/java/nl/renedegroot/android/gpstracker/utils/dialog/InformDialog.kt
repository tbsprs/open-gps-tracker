/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.utils.dialog

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment

private const val TITLE = "TITLE"
private const val MESSAGE = "MESSAGE"

fun informDialog(title: String, message: String) = InformDialog().apply {
    arguments = bundleOf(
            TITLE to title,
            MESSAGE to message
    )
}

class InformDialog : DialogFragment() {

    private val title
        get() = requireArguments().getString(TITLE)
    private val message
        get() = requireArguments().getString(MESSAGE)

    override fun onCreateDialog(savedInstanceState: Bundle?) =
            AlertDialog.Builder(requireContext())
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                    .setCancelable(true)
                    .create()
}