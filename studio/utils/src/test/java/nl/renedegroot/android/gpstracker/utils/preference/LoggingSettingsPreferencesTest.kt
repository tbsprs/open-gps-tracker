package nl.renedegroot.android.gpstracker.utils.preference

import junit.framework.TestCase
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.FiveMinutes
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval.Second
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`


class LoggingSettingsPreferencesTest : TestCase() {

    fun testClampInterval() {
        assertThat( clampInterval(-1), `is`(Second.intervalTime))
        assertThat( clampInterval(10000), `is`(FiveMinutes.intervalTime))
        assertThat( clampInterval(30), `is`(LoggingInterval.HalfMinute.intervalTime))
        assertThat( clampInterval(49), `is`(49))
    }
}