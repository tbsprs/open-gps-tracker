/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.utils.formatting

import android.content.Context
import android.content.res.Resources
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.test.utils.R
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import java.util.Calendar
import java.util.Locale

class StatisticsFormatterTest {

    private val SIX_SECONDS = 6 * 1000L
    private val FIVE_MINUTES = ((5 * 60) + 4) * 1000L
    private val TWO_HOURS = 2 * 60 * 60 * 1000L
    private val THREE_DAYS = 3 * 24 * 60 * 60 * 1000L
    private val context: Context = mock()

    private val resources: Resources = mock()
    private val timeSpanCalculator: TimeSpanCalculator = mock()
    private val preferences: UnitSettingsPreferences = mock {
        whenever(it.mpsToSpeed).thenReturn(3.6F)
    }
    private val localeProvider: LocaleProvider = mock()
    private var referenceDate = Calendar.getInstance()!!
    lateinit var sut: StatisticsFormatter

    @Before
    fun setup() {
        val sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        this.sut = sut

        whenever(context.getString(R.string.format_runners_speed)).thenReturn("%1\$d:%2\$02d %3\$s")
        whenever(context.getString(R.string.empty_dash)).thenReturn("-")

        whenever(context.getString(R.string.units__metrics_format_big_100_kilometer)).thenReturn("%.0f kilometer")
        whenever(context.getString(R.string.units__metrics_format_big_kilometer)).thenReturn("%.2f kilometer")
        whenever(context.getString(R.string.units__metrics_format_small_meters)).thenReturn("%.1f meter")
        whenever(context.getString(R.string.units__metrics_format_small_100_meters)).thenReturn("%.0f meter")

        whenever(context.resources).thenReturn(resources)
        whenever(resources.getQuantityString(R.plurals.track_duration_seconds, 1, 1)).thenReturn("1 second")
        whenever(resources.getQuantityString(R.plurals.track_duration_seconds, 6, 6)).thenReturn("6 seconds")
        whenever(resources.getQuantityString(R.plurals.track_duration_minutes, 1, 1)).thenReturn("1 minute")
        whenever(resources.getQuantityString(R.plurals.track_duration_minutes, 5, 5)).thenReturn("5 minutes")
        whenever(resources.getQuantityString(R.plurals.track_duration_hours, 2, 2)).thenReturn("2 hours")
        whenever(resources.getQuantityString(R.plurals.track_duration_hours, 1, 1)).thenReturn("1 hour")
        whenever(resources.getQuantityString(R.plurals.track_duration_days, 1, 1)).thenReturn("1 day")
        whenever(resources.getQuantityString(R.plurals.track_duration_days, 3, 3)).thenReturn("3 days")
        whenever(resources.getString(R.string.speed_runners_unit)).thenReturn("min")

        referenceDate = Calendar.getInstance()
        referenceDate.set(Calendar.YEAR, 2016)
        referenceDate.set(Calendar.MONTH, 11)
        referenceDate.set(Calendar.DAY_OF_MONTH, 6)
        referenceDate.set(Calendar.HOUR_OF_DAY, 11)
        referenceDate.set(Calendar.MINUTE, 8)
    }

    fun setupMetrics() {
        whenever(preferences.useMetrics).thenReturn(true)
        whenever(preferences.mpsToSpeed).thenReturn(3.6F)
        whenever(preferences.meterToBigDistance).thenReturn(1000.0F)
        whenever(preferences.meterToSmallDistance).thenReturn(1F)
        whenever(preferences.speedUnit).thenReturn("kph")
    }

    @Test
    fun testConvertMetersToDistanceFewMeters() {
        setupMetrics()
        // Act
        val distance = sut.convertMetersToDistance(context, 1.1234566F)
        // Assert
        assertThat(distance, `is`("1.1 meter"))
    }

    @Test
    fun testConvertMetersToDistanceBunchOfMeters() {
        setupMetrics()
        // Act
        val distance = sut.convertMetersToDistance(context, 123.123456F)
        // Assert
        assertThat(distance, `is`("123 meter"))
    }

    @Test
    fun testConvertMetersToDistanceManyMeters() {
        setupMetrics()
        // Act
        val distance = sut.convertMetersToDistance(context, 12345.123456F)
        // Assert
        assertThat(distance, `is`("12.35 kilometer"))
    }

    @Test
    fun testConvertMetersToDistanceLotsAnLotsOfMeters() {
        setupMetrics()
        // Act
        val distance = sut.convertMetersToDistance(context, 123452.123456F)
        // Assert
        assertThat(distance, `is`("123 kilometer"))
    }

    @Test
    fun testConvertOneSecond() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, SIX_SECONDS / 6L)
        //
        assertThat(duration, `is`("1 second"))
    }

    @Test
    fun testConvertSeconds() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, SIX_SECONDS)
        //
        assertThat(duration, `is`("6 seconds"))
    }

    @Test
    fun testConvertOneMinute() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, FIVE_MINUTES / 5L + SIX_SECONDS)
        //
        assertThat(duration, `is`("1 minute"))
    }

    @Test
    fun testConvertMinutes() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, FIVE_MINUTES + SIX_SECONDS)
        //
        assertThat(duration, `is`("5 minutes"))
    }

    @Test
    fun testConvertHoursAndMinutes() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, TWO_HOURS + FIVE_MINUTES)
        //
        assertThat(duration, `is`("2 hours 5 minutes"))
    }

    @Test
    fun testConvertOneHour() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, TWO_HOURS / 2L)
        //
        assertThat(duration, `is`("1 hour"))
    }

    @Test
    fun testConvertOnyHours() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, TWO_HOURS)
        //
        assertThat(duration, `is`("2 hours"))
    }


    @Test
    fun testConvertDaysAndHours() {
        // Act
        val duration = sut.convertSpanDescriptiveDuration(context, THREE_DAYS + TWO_HOURS + FIVE_MINUTES)
        //
        assertThat(duration, `is`("3 days 2 hours"))
    }

    @Test
    fun testNone() {
        // Arrange
        val timestamp: Long? = null
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("-"))
    }

    @Test
    fun testToday() {
        // Arrange
        val calendar = referenceDate.clone() as Calendar
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 45)
        val timestamp = calendar.timeInMillis
        whenever(timeSpanCalculator.getRelativeTimeSpanString(timestamp)).thenReturn("1 hour ago")
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("1 hour ago"))
    }


    @Test
    fun testYesterday() {
        // Arrange
        val calendar = referenceDate.clone() as Calendar
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 45)
        calendar.add(Calendar.DAY_OF_YEAR, -1)
        val timestamp = calendar.timeInMillis
        whenever(timeSpanCalculator.getRelativeTimeSpanString(timestamp)).thenReturn("yesterday")
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("yesterday"))
    }

    @Test
    fun testOnWeekAgo() {
        // Arrange
        val calendar = referenceDate.clone() as Calendar
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 45)
        calendar.add(Calendar.DAY_OF_YEAR, -7)
        val timestamp = calendar.timeInMillis
        whenever(timeSpanCalculator.getRelativeTimeSpanString(timestamp)).thenReturn("Nov 29, 2016")
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("Nov 29, 2016"))
    }

    @Test
    fun testMoreThenAWeekAgo() {
        // Arrange
        val calendar = referenceDate.clone() as Calendar
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 45)
        calendar.add(Calendar.DAY_OF_YEAR, -7)
        val timestamp = calendar.timeInMillis
        whenever(timeSpanCalculator.getRelativeTimeSpanString(timestamp)).thenReturn("Nov 29, 2016")
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("Nov 29, 2016"))
    }

    @Test
    fun testMoreThenAYearAgo() {
        // Arrange
        val calendar = referenceDate.clone() as Calendar
        calendar.set(Calendar.HOUR_OF_DAY, 9)
        calendar.set(Calendar.MINUTE, 45)
        calendar.add(Calendar.YEAR, -1)
        val timestamp = calendar.timeInMillis
        whenever(timeSpanCalculator.getRelativeTimeSpanString(timestamp)).thenReturn("Dec 6, 2015")
        // Act
        val timeName = sut.convertTimestampToStart(context, timestamp)
        // Assert
        assertThat(timeName, `is`("Dec 6, 2015"))
    }

    @Test
    fun testMPStoOneKMP() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        whenever(localeProvider.locale).thenReturn(Locale.GERMAN)
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 1000.0F / 3600, true)
        // Assert
        assertThat(speed, `is`("1,00 kph"))
    }

    @Test
    fun testMPStoTenKMP() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        whenever(localeProvider.locale).thenReturn(Locale.GERMAN)
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 10000.0F / 3600, true)
        // Assert
        assertThat(speed, `is`("10,0 kph"))
    }

    @Test
    fun testMPStoThreeKMP() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 10000.0F / 10800, false)
        // Assert
        assertThat(speed, `is`("3.33 kph"))
    }

    @Test
    fun `speed without decimals`() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 10000.0F / 10800, false, decimals = false)
        // Assert
        assertThat(speed, `is`("3 kph"))
    }

    @Test
    fun `convert 10 kmp to 6 minutes per kilometer`() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        whenever(preferences.useInverseRunningSpeed).thenReturn(true)
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 10000.0F / 3600, true)
        // Assert
        assertThat(speed, `is`("6:00 min"))
    }

    @Test
    fun `convert to 7 min-km not 6-60 min-km`() {
        // Arrange
        sut = StatisticsFormatter(localeProvider, preferences, timeSpanCalculator)
        setupMetrics()
        whenever(preferences.useInverseRunningSpeed).thenReturn(true)
        // Act
        val speed = sut.convertMeterPerSecondsToSpeed(context, 2.381F, true)
        // Assert
        assertThat(speed, `is`("7:00 min"))
    }
}
