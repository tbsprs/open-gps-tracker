package nl.renedegroot.android.gpstracker.integration;

import android.net.Uri;

oneway interface IGPSLoggerStateCallback {

    void didStopLogging();
    void didPauseLogging(in Uri trackUri);
    void didStartLogging(in Uri trackUri);
}
