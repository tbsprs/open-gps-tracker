package nl.renedegroot.android.gpstracker.integration;

import android.net.Uri;
import android.location.Location;
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerStateCallback;

interface IGPSLoggerServiceRemote {

	int loggingState();
    long getTrackId();
    Uri getLastWaypoint();
    void addObserver(IGPSLoggerStateCallback callback);
    void removeObserver(IGPSLoggerStateCallback callback);
}
