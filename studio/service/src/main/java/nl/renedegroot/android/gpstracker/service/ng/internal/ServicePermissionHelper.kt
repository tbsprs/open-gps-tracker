/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.Manifest
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import timber.log.Timber
import javax.inject.Inject

internal class ServicePermissionHelper @Inject constructor(private val context: Context, val notification: LoggerNotification) {

    fun withCheckedPermission(permission: String = Manifest.permission.ACCESS_FINE_LOCATION, intent: Intent? = null, function: () -> Unit) {
        if (PermissionChecker.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, permission)) {
            Timber.d("Missing permission $permission")
            if (intent != null) {
                notification.blockedByPermission(intent)
            }
        } else {
            function.invoke()
        }
    }
}
