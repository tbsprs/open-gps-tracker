/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.content.SharedPreferences
import android.net.Uri
import androidx.core.content.edit
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_LOGGING
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_PAUSED
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_STOPPED

private const val LOGGING_STATE = "PREFERENCE_LOGGING_STATE"
private const val TRACK_ID = "PREFERENCE_LOGGING_STATE_TRACK_ID"
private const val SEGMENT_ID = "PREFERENCE_LOGGING_STATE_SEGMENT_ID"
private const val WAYPOINT = "PREFERENCE_LOGGING_STATE_WAYPOINT"
private const val INTERVAL = "PREFERENCE_LOGGING_STATE_INTERVAL"

internal class StatePersistence(
        preferencesProvider: () -> SharedPreferences
):StatePersistenceInterface {
    private val preferences by lazy(preferencesProvider)

    override fun saveLogState(logState: LogState) =
            preferences.edit {
                putInt(LOGGING_STATE, logState.code)
                when (logState) {
                    LogState.Stopped -> Unit
                    is LogState.Paused -> {
                        putLong(TRACK_ID, logState.trackId)
                        putLong(SEGMENT_ID, logState.segmentId)
                        putString(WAYPOINT, logState.lastWaypoint.toString())
                    }
                    is LogState.Logging -> {
                        putLong(TRACK_ID, logState.trackId)
                        putLong(SEGMENT_ID, logState.segmentId)
                        putString(WAYPOINT, logState.lastWaypoint.toString())
                    }
                }
            }

    override fun loadLogState() =
            when (preferences.getInt(LOGGING_STATE, STATE_STOPPED)) {
                STATE_LOGGING -> LogState.Logging(preferences.getTrackId(), preferences.getSegmentId(), preferences.getLastWaypoint(), preferences.getInterval())
                STATE_PAUSED -> LogState.Paused(preferences.getTrackId(), preferences.getSegmentId(), preferences.getLastWaypoint(), preferences.getInterval())
                STATE_STOPPED -> LogState.Stopped
                else -> LogState.Stopped
            }
}

private fun SharedPreferences.getLastWaypoint(): Uri = Uri.parse(getString(WAYPOINT, "content://"))
private fun SharedPreferences.getTrackId(): Long = getLong(TRACK_ID, -1L)
private fun SharedPreferences.getSegmentId(): Long = getLong(SEGMENT_ID, -1L)
private fun SharedPreferences.getInterval(): Int = getInt(INTERVAL, -1)

