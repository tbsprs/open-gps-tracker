/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.service.db

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Segments
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Segments.TRACK
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints.SEGMENT
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Waypoints._ID
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.TracksColumns
import nl.renedegroot.android.gpstracker.service.util.readName
import nl.renedegroot.android.gpstracker.service.util.trackUri
import timber.log.Timber

fun SQLiteDatabase.splitTrackOnWaypoint(trackId: Long, splitWaypointId: Long): Uri? {
    return transact {
        val timeQuery = query(Waypoints.WAYPOINTS, arrayOf(Waypoints.TIME), "$_ID = ? ", arrayOf(splitWaypointId.toString()), null, null, null)
        if (!timeQuery.moveToFirst()) {
            Timber.e("Split failed missing waypoint $splitWaypointId in track $trackId")
            null
        } else {
            val time = timeQuery.getLong(0)
            val insertedTrackId = insert(
                    ContentConstants.Tracks.TRACKS,
                    null,
                    ContentValues().apply {
                        put(TracksColumns.NAME, trackUri(trackId).readName())
                        put(TracksColumns.CREATION_TIME, time)
                    }
            )
            val segmentIds = query(Segments.SEGMENTS, arrayOf(Segments._ID), "$TRACK = ?", arrayOf(trackId.toString()), "", "", Segments._ID)
            segmentIds.forEach {
                val segmentId = segmentIds.getLong(0)
                val waypointIds = query(Waypoints.WAYPOINTS, arrayOf(_ID), "$SEGMENT = ?", arrayOf(segmentId.toString()), "", "", _ID)
                if (waypointIds.moveToLast()) {
                    // Leave all the old waypoints of the segment in the old track
                    val lastId = waypointIds.getLong(0)
                    if (lastId < splitWaypointId) {
                    } else if (waypointIds.moveToFirst()) {
                        //  Create a new segment and fill it with waypoints
                        val insertedSegmentId = insert(
                                ContentConstants.Segments.SEGMENTS,
                                null,
                                ContentValues().apply { put(TRACK, insertedTrackId) }
                        )
                        update(
                                Waypoints.WAYPOINTS,
                                ContentValues().apply { put(Waypoints.SEGMENT, insertedSegmentId) },
                                "$SEGMENT = ? and $_ID >= ?",
                                arrayOf(segmentId.toString(), splitWaypointId.toString())
                        )
                    }
                }
            }
            trackUri(insertedTrackId)
        }
    }
}

private fun Cursor.forEach(action: (Cursor) -> Unit) {
    if (moveToFirst()) {
        do {
            action(this)
        } while (moveToNext())
    }
}

private fun <T> SQLiteDatabase.transact(transaction: () -> T): T {
    beginTransaction()
    try {
        val value = transaction.invoke()
        setTransactionSuccessful()
        return value
    } finally {
        if (inTransaction()) {
            endTransaction()
        }
    }
}