package nl.renedegroot.android.gpstracker.service.ng.internal

internal interface GpsListener {

    fun startListener(gpsLogger: GpsLogger, interval: Int)

    fun stopListener()
}