/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.service.dagger

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.service.ng.CommandFactory
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsExecutor
import nl.renedegroot.android.gpstracker.service.ng.internal.GpsLogger
import nl.renedegroot.android.gpstracker.service.ng.internal.LocationFilter
import nl.renedegroot.android.gpstracker.service.ng.internal.LoggerNotification
import nl.renedegroot.android.gpstracker.service.ng.internal.PowerManager
import nl.renedegroot.android.gpstracker.service.ng.internal.TrackPersistence
import javax.inject.Named

@Module
class ServiceModule(val service: Service) {

    @Provides
    fun context(): Context = service

    @Provides
    fun serviceCommander() = ServiceCommander(service)

    @Provides
    @Named("providerAuthority")
    fun providerAuthority() =
            ContentConstants.GPS_TRACKS_AUTHORITY

    @Provides
    internal fun trackPersistence() = TrackPersistence(service.contentResolver)

    @Provides
    @ServiceScope
    internal fun loggerNotification(commandFactory: CommandFactory): LoggerNotification {
        val notificationManager = service.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        return LoggerNotification(notificationManager, service.resources, service, commandFactory)
    }

    @Provides
    fun commandFactory() = CommandFactory(service)

    @Provides
    internal fun locationFilter() = LocationFilter()

    @Provides
    internal fun powerManager() = PowerManager(service)

    @Provides
    @ServiceScope
    internal fun gpsExecutor(gpsLogger: GpsLogger) = GpsExecutor(gpsLogger)
}
