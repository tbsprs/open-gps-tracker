/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.service.integration

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.IBinder
import android.os.RemoteException
import androidx.core.content.ContextCompat
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerServiceRemote
import nl.renedegroot.android.gpstracker.integration.IGPSLoggerStateCallback
import timber.log.Timber
import javax.inject.Inject

/**
 * Class to interact with the service that tracks and logs the locations
 */
class ServiceManager @Inject constructor(
        private val context: Context
) {

    private val mStartLock = Any()
    private var mGPSLoggerRemote: IGPSLoggerServiceRemote? = null
    private var mBound = false

    /**
     * Class for interacting with the main interface of the service.
     */
    private var mServiceConnection: ServiceConnection? = null
    private var mOnServiceConnected: (() -> Unit)? = null
    private val commander: ServiceCommander = ServiceCommander(context)

    fun getLoggingState() = remoteExecute(ServiceConstants.STATE_UNKNOWN) {
        it.loggingState()
    }

    fun getTrackId() = remoteExecute(-1L) {
        it.trackId
    }

    fun getLastWaypoint() = remoteExecute(null) {
        it.lastWaypoint
    }

    fun addObserver(callback: IGPSLoggerStateCallback) = remoteExecute(Unit) {
        it.addObserver(callback)
    }

    fun removeObserver(callback: IGPSLoggerStateCallback) = remoteExecute(Unit) {
        it.removeObserver(callback)
    }

    fun startGPSLogging(trackName: String, interval: Int) {
        commander.startGPSLogging(trackName, interval)
    }

    fun stopGPSLogging() {
        commander.stopGPSLogging()
    }

    fun pauseGPSLogging() {
        commander.pauseGPSLogging()
    }

    fun resumeGPSLogging() {
        commander.resumeGPSLogging()
    }

    /**
     * Means by which an Activity lifecycle aware object hints about binding and unbinding
     *
     * @param onServiceConnected Run on main thread after the service is bound
     */
    fun startup(onServiceConnected: () -> Unit) {
        synchronized(mStartLock) {
            if (!mBound) {
                mOnServiceConnected = onServiceConnected
                mServiceConnection = object : ServiceConnection {
                    override fun onServiceConnected(className: ComponentName, service: IBinder) {
                        synchronized(mStartLock) {
                            this@ServiceManager.mGPSLoggerRemote = IGPSLoggerServiceRemote.Stub.asInterface(service)
                            mBound = true
                        }
                        if (mOnServiceConnected != null) {
                            mOnServiceConnected?.invoke()
                            mOnServiceConnected = null
                        }
                    }

                    override fun onServiceDisconnected(className: ComponentName) {
                        synchronized(mStartLock) {
                            mBound = false
                        }
                    }
                }.apply {
                    context.bindService(commander.createServiceIntent(), this, Context.BIND_AUTO_CREATE)
                }
            } else {
                Timber.w("Attempting to connect whilst already connected")
            }
        }
    }

    /**
     * Means by which an Activity lifecycle aware object hints about binding and unbinding
     */
    fun shutdown() {
        synchronized(mStartLock) {
            try {
                if (mBound) {
                    context.unbindService(this.mServiceConnection!!)
                    this@ServiceManager.mGPSLoggerRemote = null
                    mServiceConnection = null
                    mBound = false
                }
            } catch (e: IllegalArgumentException) {
                Timber.w(e, "Failed to unbind a service, perhaps the service disappeared?")
            }

        }
    }

    private fun <T> remoteExecute(default: T, block: (IGPSLoggerServiceRemote) -> T): T {
        synchronized(mStartLock) {
            try {
                if (mBound) {
                    return block(mGPSLoggerRemote!!)
                } else {
                    Timber.w("Remote interface to logging service not found. Started: %s", mBound)
                }
            } catch (e: RemoteException) {
                Timber.e(e, "Could start GPSLoggerService.")
            }
        }
        return default
    }
}
