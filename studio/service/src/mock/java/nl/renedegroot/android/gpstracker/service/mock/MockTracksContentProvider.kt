/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.service.mock

import android.content.Context
import android.net.Uri
import androidx.core.content.contentValuesOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.service.BuildConfig
import nl.renedegroot.android.gpstracker.service.db.DatabaseConstants.Tracks
import nl.renedegroot.android.gpstracker.service.db.DatabaseHelper
import nl.renedegroot.android.gpstracker.service.db.GPStrackingProvider
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Segments
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import java.util.*

class MockTracksContentProvider : GPStrackingProvider() {

    private var needReset = true
    private val requireContext: Context
        get() = checkNotNull(context) { "Context required" }

    override fun onCreate(): Boolean {
        if (needReset) {
            needReset = false
            context?.deleteDatabase(DatabaseHelper(context).databaseName)
            asyncLoadZigZagAmsterdam()
        }
        return super.onCreate()
    }

    private fun asyncLoadZigZagAmsterdam() {
        GlobalScope.launch(Dispatchers.IO) {
            loadZigZagAmsterdam()
        }
    }

    private fun loadZigZagAmsterdam() {
        // Some random picked points in Amsterdam, NL
        val gpxAmsterdam = listOf(
                Pair(52.3770600, 4.8984461),
                Pair(52.3763940, 4.8972632),
                Pair(52.3762200, 4.9028743),
                Pair(52.3740490, 4.8999434),
                Pair(52.3732520, 4.8940387),
                Pair(52.3746273, 4.8936862),
                Pair(52.3757935, 4.8913026),
                Pair(52.3743277, 4.8904614),
                Pair(52.3732363, 4.8899005),
                Pair(52.3720379, 4.8910572),
                Pair(52.3719737, 4.8950006),
                Pair(52.3709465, 4.8950006),
                Pair(52.3708181, 4.8911098),
                Pair(52.3704328, 4.8876747),
                Pair(52.3701011, 4.8876747),
                Pair(52.3691915, 4.8869211),
                Pair(52.3688705, 4.8919686),
                Pair(52.3695768, 4.8935810),
                Pair(52.3691808, 4.8993471),
                Pair(52.3672011, 4.9051132),
                Pair(52.3661524, 4.9119834),
                Pair(52.3664734, 4.9212022)
        )
        requireContext.contentResolver.insert(
                Uri.Builder()
                        .scheme("content")
                        .authority(BuildConfig.providerAuthority)
                        .appendPath(ContentConstants.Tracks.TRACKS)
                        .build(),
                contentValuesOf(Tracks.NAME to "Zigzag Amsterdam")
        )?.let { track ->
            requireContext.contentResolver.insert(track.append(Segments.SEGMENTS), null)?.let { segment ->
                val waypoints = segment.append(Waypoints.WAYPOINTS)
                val now = Date().time
                for (i in gpxAmsterdam.indices) {
                    val time = now - (gpxAmsterdam.size - i) * 10_0000L + ((Math.random() * 4_000).toLong() - 2_000L)
                    requireContext.contentResolver.insert(
                            waypoints,
                            contentValuesOf(
                                    Waypoints.LATITUDE to gpxAmsterdam[i].first,
                                    Waypoints.LONGITUDE to gpxAmsterdam[i].second,
                                    Waypoints.TIME to time
                            )
                    )
                }
            }
        }

    }
}

