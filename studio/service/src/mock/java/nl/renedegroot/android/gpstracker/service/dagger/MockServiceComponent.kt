package nl.renedegroot.android.gpstracker.service.dagger

import dagger.Component

@ServiceScope
@Component(modules = [ServiceModule::class, MockSystemModule::class])
interface MockServiceComponent : ServiceComponent
