/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note

import android.content.Context
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.content.ContentController
import nl.renedegroot.android.gpstracker.service.util.mediaTrackUri
import nl.renedegroot.android.opengpstrack.ng.note.internal.NoteManager

class NoteMarkerController(
        context: Context,
        private val scope: CoroutineScope = CoroutineScope(Dispatchers.Default),
        private val _markers: MutableLiveData<List<NoteMarker>> = MutableLiveData()
) : CoroutineScope by scope {

    private val pin = checkNotNull(ContextCompat.getDrawable(context, R.drawable.notes__pin)).toBitmap(128, 128)
    private val contentController = ContentController(context.applicationContext)
    private val noteManager = NoteManager(context.applicationContext)
    private var lastUpdate: Job? = null
    private val listener = object : ContentController.Listener {
        override fun onChangeUriContent(contentUri: Uri, changesUri: Uri) {
            updateMarkers(contentUri)
        }
    }

    val markers: LiveData<List<NoteMarker>> = _markers

    fun startOnTrack(trackUri: Uri) {
        contentController.unregisterObserver()
        val trackId = checkNotNull(trackUri.lastPathSegment).toLong()
        val mediaUri = mediaTrackUri(trackId)
        contentController.registerObserver(listener, mediaUri)
        updateMarkers(mediaUri)
    }

    private fun updateMarkers(mediaUri: Uri) {
        lastUpdate?.cancel()
        lastUpdate = launch(Dispatchers.IO) {

            val notes = noteManager.listNotes(mediaUri)
            _markers.postValue(notes.map { (uri, location) ->
                val options = MarkerOptions()
                        .position(LatLng(location.latitude, location.longitude))
                        .icon(BitmapDescriptorFactory.fromBitmap(pin))
                NoteMarker(uri, options)
            })
        }
    }

    fun stop() {
        contentController.unregisterObserver()
        _markers.value?.map { it.removeFromMap() }
    }
}

data class NoteMarker(val uri: Uri, private val markerOptions: MarkerOptions) {
    private var marker: Marker? = null

    fun removeFromMap() {
        marker?.remove()
    }

    fun addToMap(googleMap: GoogleMap): Marker {
        val marker = googleMap.addMarker(markerOptions)
        this.marker = marker
        return marker
    }
}
