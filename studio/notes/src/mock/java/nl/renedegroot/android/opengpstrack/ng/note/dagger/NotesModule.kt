/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.opengpstrack.ng.note.dagger

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.opengpstrack.ng.note.internal.NotePresenter
import java.io.File
import java.io.FileOutputStream

@Module
class NotesModule {

    @Provides
    fun provideImagePicker() = { fragment: Fragment, _: Int ->
        val context = fragment.requireContext()
        val file = File(context.cacheDir, "mountain_picture.jpg")
        context.assets.open("mountain.jpg").use {
            it.copyTo(FileOutputStream(file))
        }

        ViewModelProvider(fragment).get(NotePresenter::class.java)
                .onImagesPicked(listOf(file))
    }
}