/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.base.dagger

import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import dagger.Component
import nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus.GpsStatusController
import nl.renedegroot.android.gpstracker.ng.base.location.LocationFactory
import java.text.SimpleDateFormat
import java.util.concurrent.Executor
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [BaseModule::class, SystemBaseModule::class])
interface BaseComponent {

    @Named("dayFormatter")
    fun dayFormatter(): SimpleDateFormat

    @DiskIO
    fun diskExecutor(): Executor

    @NetworkIO
    fun networkExecutor(): Executor

    @Computation
    fun computationExecutor(): Executor

    @UIUpdate
    fun uiUpdateExecutor(): Executor

    fun applicationContext(): Context

    fun locationFactory(): LocationFactory

    fun contentResolver(): ContentResolver

    fun packageManager(): PackageManager

    fun gpsStatusController(): GpsStatusController
}
