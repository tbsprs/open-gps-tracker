/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.location.GnssStatus
import android.os.Build.VERSION_CODES.N
import androidx.annotation.RequiresPermission
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread

@TargetApi(N)
class GnnsStatusControllerImpl(
        context: Context
) : BaseGpsStatusControllerImpl(context) {

    private val callback = object : GnssStatus.Callback() {
        override fun onStarted() {
            started()
        }

        override fun onStopped() {
            stopped()
        }

        override fun onFirstFix(ttffMillis: Int) {
            firstFix()
        }

        override fun onSatelliteStatusChanged(status: GnssStatus) {
            val used = (0 until status.satelliteCount).count { index -> status.usedInFix(index) }
            satellites(used, status.satelliteCount)
        }
    }

    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    override fun startUpdates(listener: GpsStatusController.Listener) {
        super.listener = listener
        onMainThread {
            locationManager.registerGnssStatusCallback(callback)
        }
    }

    override fun stopUpdates() {
        super.listener = null
        stopped()
        locationManager.unregisterGnssStatusCallback(callback)
    }

}
