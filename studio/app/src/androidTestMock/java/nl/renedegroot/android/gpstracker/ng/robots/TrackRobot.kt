/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.robots

import android.app.Activity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.test.espresso.Espresso.onIdle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.longClick
import androidx.test.espresso.matcher.RootMatchers.isPlatformPopup
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withParent
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.google.android.gms.maps.MapView
import nl.renedegroot.android.gpstracker.ng.util.IdlingMapResource
import nl.renedegroot.android.gpstracker.v2.R
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.anyOf
import org.hamcrest.Matchers.instanceOf
import java.lang.Exception

fun tracking(activity: Activity, block: TrackRobot.() -> TrackRobot): Unit =
        TrackRobot(activity)
                .start()
                .block()
                .stop()

class TrackRobot(private val activity: Activity) : Robot<TrackRobot>("TrackScreen") {

    private var resource: IdlingMapResource? = null

    fun editTrack(): TrackRobot {
        onView(allOf(withParent(withId(R.id.toolbar)), instanceOf(AppCompatImageButton::class.java)))
                .perform(click());

        return this
    }

    fun openSettings() = apply {
        try {
            viewClick(R.id.action_settings)
        } catch (e: NoMatchingViewException) {
            openActionBarOverflowOrOptionsMenu(activity)
            onView(withText(R.string.action_settings))
                    .perform(click())
        }
    }

    fun openTrackTypeSpinner(): TrackRobot {
        onView(withId(R.id.spinner))
                .perform(click())

        return this
    }

    fun selectWalking(): TrackRobot {
        onView(withText(R.string.track_type_walk))
                .inRoot(isPlatformPopup())
                .perform(click())

        return this
    }

    fun ok(): TrackRobot {
        onView(withText(android.R.string.ok))
                .perform(click())

        return this
    }

    fun openAbout(): TrackRobot {
        openActionBarOverflowOrOptionsMenu(activity)
        onView(withText(R.string.action_about))
                .perform(click())

        return this
    }

    fun openGraph(): TrackRobot {
        onView(anyOf(withId(R.id.action_graphs), withText(R.string.action_graphs)))
                .perform(click())

        return this
    }

    fun openTrackList(): TrackRobot {
        onView(anyOf(withId(R.id.action_list), withText(R.string.action_list)))
                .perform(click())

        return this
    }

    fun openSummary() = apply {
        viewClick(R.id.fragment_recording)
    }

    fun startRecording(): TrackRobot {
        onView(withContentDescription(R.string.control_record))
                .perform(click())

        return this
    }

    fun pauseRecording(): TrackRobot {
        onView(withContentDescription(R.string.control_pause))
                .perform(click())

        return this
    }

    fun resumeRecording(): TrackRobot {
        onView(withContentDescription(R.string.control_resume))
                .perform(click())

        return this
    }

    fun stopRecording(): TrackRobot {
        onView(withContentDescription(R.string.control_stop))
                .perform(click())

        return this
    }

    fun createNote() = apply {
        onView(withId(R.id.fragment_map_mapview))
                .perform(longClick())
    }

    fun start(): TrackRobot {
        val mapView = activity.findViewById<MapView>(R.id.fragment_map_mapview)
        resource = IdlingMapResource(mapView)
        IdlingRegistry.getInstance().register(resource)

        return this
    }

    fun stop() {
        onIdle {
            resource?.let {
                IdlingRegistry.getInstance().unregister(resource)
            }
        }
    }

}
