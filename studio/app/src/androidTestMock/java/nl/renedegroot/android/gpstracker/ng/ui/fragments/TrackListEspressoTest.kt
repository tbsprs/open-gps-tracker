/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.ui.fragments

import android.Manifest
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.rule.GrantPermissionRule
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListActivity
import nl.renedegroot.android.gpstracker.ng.robots.trackList
import org.junit.Rule
import org.junit.Test

class TrackListEspressoTest {

    @get:Rule
    val permissionRule: GrantPermissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    @get:Rule
    val wrapper = IntentsTestRule(TrackListActivity::class.java)

    @Test
    fun testResumeStartsPresenter() {
        // Verify
        trackList {
            isTrackListDisplayed()
        }
    }

    @Test
    fun testImportTrack() {
        trackList {
            prepareTrackSelection()
            openImport()
            openTrackTypeSpinner()
            selectWalking()
            clickDialogOk()
        }
    }

    @Test
    fun testImportDirectory() {
        trackList {
            prepareDirectorySelection()
            openImportDirectory(wrapper.activity)
            openTrackTypeSpinner()
            selectWalking()
            clickDialogOk()
        }
    }
}