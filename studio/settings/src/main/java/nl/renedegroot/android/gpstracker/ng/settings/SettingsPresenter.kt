/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.settings

import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval
import nl.renedegroot.android.gpstracker.utils.preference.LoggingSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.gpstracker.utils.preference.UnitSystem
import nl.renedegroot.android.gpstracker.utils.preference.activeInterval
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractPresenter
import timber.log.Timber

class SettingsPresenter(
        val viewModel: SettingsViewModel,
        private val loggingPreferences: LoggingSettingsPreferences,
        private val unitPreferences: UnitSettingsPreferences,
        private val serviceCommander: ServiceCommander
) : AbstractPresenter() {

    override suspend fun onStart() {
        super.onStart()
        loggingPreferences.observeChanges { markDirty() }
        unitPreferences.observeChanges { markDirty() }
    }

    override suspend fun onChange() {
        viewModel.customInterval.set(checkNotNull(loggingPreferences.useCustomInterval))
        viewModel.selectedIntervalPeriod.set(loggingPreferences.loggingInterval)
        if (unitPreferences.isImperialCountry) {
            viewModel.localeDescription.set(StringResource(R.string.settings__system_imperial_region, arrayOf(unitPreferences.displayCountry)))
        } else {
            viewModel.localeDescription.set(StringResource(R.string.settings__system_metric_region, arrayOf(unitPreferences.displayCountry)))
        }
        viewModel.selectedUnitSystem.set(unitPreferences.unitSystem)
        viewModel.inverseRunningSpeed.set(checkNotNull(unitPreferences.useInverseRunningSpeed))
    }

    override suspend fun onStop() {
        super.onStop()
        loggingPreferences.ignoreChanges()
        unitPreferences.ignoreChanges()
    }

    fun onCustomLogging(checked: Boolean) {
        Timber.i("onCustomLogging $checked")
        loggingPreferences.useCustomInterval = checked

        serviceCommander.updateInterval(loggingPreferences.activeInterval)
    }

    fun onIntervalSelected(selection: LoggingInterval) {
        Timber.i("onIntervalSelected $selection")
        loggingPreferences.loggingInterval = selection

        serviceCommander.updateInterval(loggingPreferences.activeInterval)
    }

    fun onUnitSystemSelected(selection: UnitSystem) {
        Timber.i("onUnitSystemSelected $selection")
        unitPreferences.unitSystem = selection
    }

    fun onInverseRunningSpeed(checked: Boolean) {
        Timber.i("onInverseRunningSpeed $checked")
        unitPreferences.useInverseRunningSpeed = checked
    }
}
