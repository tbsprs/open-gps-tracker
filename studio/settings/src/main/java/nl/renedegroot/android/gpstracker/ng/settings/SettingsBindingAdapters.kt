package nl.renedegroot.android.gpstracker.ng.settings

import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval
import nl.renedegroot.android.gpstracker.utils.preference.UnitSystem

private val LoggingInterval.resId: Int
    get() = when (this) {
        LoggingInterval.Second -> R.string.settings__interval_1
        LoggingInterval.TenSeconds -> R.string.settings__interval_10
        LoggingInterval.HalfMinute -> R.string.settings__interval_30
        LoggingInterval.Minute -> R.string.settings__interval_60
        LoggingInterval.FiveMinutes -> R.string.settings__interval_300
    }
private val UnitSystem.resId: Int
    get() = when (this) {
        UnitSystem.Device -> R.string.settings__system_device
        UnitSystem.Metric -> R.string.settings__system_metric
        UnitSystem.Imperial -> R.string.settings__system_imperial
    }

@BindingAdapter("unitAdapter", "selectedUnit")
internal fun Spinner.setUnitAdapter(items: List<UnitSystem>, selection: UnitSystem?) {
    if (adapter == null) {
        adapter = ArrayAdapter(context, R.layout.settings__item_spinner, items.map { context.getString(it.resId) })
    }
    val index = items.indexOfFirst { it == selection }
    if (index != -1 && index != selectedItemPosition) {
        setSelection(index)
    }
}

@BindingAdapter("intervalAdapter", "selectedInterval")
internal fun Spinner.setIntervalAdapter(items: List<LoggingInterval>, selection: LoggingInterval?) {
    if (adapter == null) {
        adapter = ArrayAdapter(context, R.layout.settings__item_spinner, items.map { context.getString(it.resId) })
    }
    val index = items.indexOfFirst { it == selection }
    if (index != -1 && index != selectedItemPosition) {
        setSelection(index)
    }
}

@BindingAdapter("text")
internal fun TextView.setText(textResource: StringResource?) {
    text = if (textResource != null) {
        context.getString(textResource.resId, *textResource.formatArgs)
    } else {
        ""
    }
}

class StringResource(
        @StringRes val resId: Int,
        val formatArgs: Array<String>
)

