/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.permissions

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.ng.base.permissions.permissionActivityIntent
import nl.renedegroot.android.gpstracker.utils.Consumable

class PermissionNavigation {

    internal val navigation = MutableLiveData<Consumable<Navigation>>()

    fun requestPermissions(permission: List<String>) {
        navigation.postValue(Consumable(Navigation.RequestPermission(permission)))
    }
}

internal class PermissionNavigator(val activity: FragmentActivity) {

    fun observer(navigation: PermissionNavigation) {
        navigation.navigation.observe(activity) {
            it?.consume { destination -> navigate(destination) }
        }
    }

    private fun navigate(destination: Navigation) =
            when (destination) {
                is Navigation.RequestPermission -> activity.startActivity(permissionActivityIntent(activity, destination.permission))
            }
}

sealed class Navigation {
    internal class RequestPermission(val permission: List<String>) : Navigation()
}
