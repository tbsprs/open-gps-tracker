/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.permissions

import android.content.Context
import androidx.fragment.app.FragmentActivity
import javax.inject.Inject

const val ACTIVITY_RECOGNITION_GMS = "com.google.android.gms.permission.ACTIVITY_RECOGNITION"

class PermissionHelper @Inject constructor(
        private val context: Context,
        private val permissionChecker: PermissionChecker,
        private val permissionNavigation: PermissionNavigation) {

    private var didAskThisSession = false

    fun withCheckedPermission(
            requiredPermissions: List<String>,
            optionalPermissions: List<String> = emptyList(),
            function: () -> Unit) {
        val hasRequiredPermissions = permissionChecker.checkSelfPermission(context, requiredPermissions)
        val hasOptionPermissions = permissionChecker.checkSelfPermission(context, optionalPermissions)
        if (hasRequiredPermissions) {
            function.invoke()
        }

        val appPermissions = ArrayList<String>().apply {
            addAll(requiredPermissions)
            addAll(optionalPermissions)
        }.filter { !permissionChecker.checkSelfPermission(context, it) }
        if (!hasRequiredPermissions) {
            permissionNavigation.requestPermissions(appPermissions)
        } else if (!hasOptionPermissions && !didAskThisSession) {
            didAskThisSession = true
            permissionNavigation.requestPermissions(appPermissions)
        }
    }

    fun hasPermission(permission: ArrayList<String>): Boolean =
            permissionChecker.checkSelfPermission(context, permission)


    fun supportPermissionScreen(activity: FragmentActivity) {
        PermissionNavigator(activity).observer(permissionNavigation)
    }
}

