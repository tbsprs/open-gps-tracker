package nl.renedegroot.android.opengpstrack.ng.summary

import android.net.Uri
import nl.renedegroot.android.gpstracker.service.integration.ServiceCommander
import nl.renedegroot.android.gpstracker.service.integration.ServiceConstants
import nl.renedegroot.android.gpstracker.service.util.LoggingStateController
import nl.renedegroot.android.gpstracker.service.util.LoggingStateListener
import nl.renedegroot.android.gpstracker.service.util.readTrackType
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractTrackPresenter
import nl.renedegroot.android.opengpstrack.ng.summary.dagger.SummaryConfiguration
import nl.renedegroot.android.opengpstrack.ng.summary.internal.SummaryNavigation
import nl.renedegroot.android.opengpstrack.ng.summary.internal.SummaryViewModel
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import javax.inject.Inject

// public due to exposure in SummaryComponent
class SummaryPresenter : AbstractTrackPresenter(), LoggingStateListener {

    internal val viewModel = SummaryViewModel()

    @Inject
    internal lateinit var serviceCommander: ServiceCommander

    @Inject
    internal lateinit var loggingStateController: LoggingStateController

    @Inject
    internal lateinit var navigation: SummaryNavigation

    @Inject
    internal lateinit var summaryManager: SummaryManager

    init {
        SummaryConfiguration.summaryComponent.inject(this)
    }

    override suspend fun onStart() {
        super.onStart()
        loggingStateController.connect(this)
        summaryManager.start()
    }

    override suspend fun onChange() {
        trackUri?.let {
            summaryManager.collectSummaryInfo(it) { summary ->
                viewModel.showSummary(summary)
            }
            viewModel.trackIcon.set(it.readTrackType().drawableId)
        }
    }

    override suspend fun onStop() {
        super.onStop()
        loggingStateController.disconnect()
        stopContentUpdates()
        summaryManager.stop()
    }

    fun onPauseResume() =
            when (loggingStateController.loggingState) {
                ServiceConstants.STATE_PAUSED -> serviceCommander.resumeGPSLogging()
                ServiceConstants.STATE_LOGGING -> serviceCommander.pauseGPSLogging()
                else -> Unit
            }

    fun onUpNavigation() = navigation.stop()

    fun onSystemVisibility() = viewModel.immersive.set(viewModel.immersive.get().not())

    override fun didStopLogging() {
        navigation.stop()
    }

    override fun didPauseLogging(trackUri: Uri) {
        viewModel.trackingControl.set(R.string.logcontrol_resume)
    }

    override fun didStartLogging(trackUri: Uri) {
        viewModel.trackingControl.set(R.string.logcontrol_pause)
    }

    private fun stopContentUpdates() {
        contentController.unregisterObserver()
    }
}
