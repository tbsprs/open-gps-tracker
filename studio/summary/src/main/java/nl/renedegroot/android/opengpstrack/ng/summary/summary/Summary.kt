/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.opengpstrack.ng.summary.summary

import android.net.Uri
import com.google.android.gms.maps.model.LatLngBounds
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.service.util.Waypoint

data class Summary(val trackUri: Uri,
                   val name: String,
                   val type: TrackTypeDescriptions.TrackType,
                   val startTimestamp: Long,
                   val endTimestamp: Long,
                   val trackedPeriod: Long,
                   val distance: Float,
                   val maxSpeed: Float,
                   val bounds: LatLngBounds,
                   val waypoints: List<List<Waypoint>>,
                   val deltas: List<List<Delta>>) {
    val count: Int
        get() = waypoints.fold(0) { count, list -> count + list.size }

    data class Delta(val totalMilliseconds: Long,
                     val totalMeters: Float,
                     val deltaMeters: Float,
                     val deltaMilliseconds: Long,
                     val altitude: Double)
}

fun Summary.Delta.speed() =
        deltaMeters / (deltaMilliseconds / 1000F)

fun List<Summary.Delta>.speed() =
        sumByFloat { it.deltaMeters } / (sumBy { it.deltaMilliseconds.toInt() } / 1000F)

inline fun <T> Iterable<T>.sumByFloat(selector: (T) -> Float): Float {
    var sum = 0.0f
    for (element in this) {
        sum += selector(element)
    }
    return sum
}
