package nl.renedegroot.android.opengpstrack.ng.summary.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryCalculator
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager

@Module
class SummaryModule(context: Context) {

    private val applicationContext = context.applicationContext

    @Provides
    @SummaryScope
    fun providesSummaryManager(calculator: SummaryCalculator) = SummaryManager(applicationContext, calculator)

    @Provides
    fun providesSummaryCalculator() = SummaryCalculator(applicationContext)
}
