/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.v2.wear

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt

class WearViewModel {

    val duration = ObservableField<Long>(0L)
    val distance = ObservableField<Float>(0F)
    val averageSpeed = ObservableField<Float>(0F)
    val inverseSpeed = ObservableBoolean(false)

    val state = ObservableField<Control>(Control.Sync())
    val manualRefresh = ObservableBoolean(false)
    val confirmAction = ObservableField<Control?>()

    val leftControl = ObservableField<Control>(Control.Stop(false))
    val rightControl = ObservableField<Control>(Control.Start(false))
    val bottomControl = ObservableField<Control>(Control.Pause(false))

    val ambient = ObservableBoolean(false)
    val scrollToPage = ObservableInt(0)
}
